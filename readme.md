# Redes img

> Assets de PlanetMinecraft para mi perfil

## Default Social


```
[style b size=42px color=#18bc9b]About me:[/style]

[img title=img1 width=300 height=197]https://gitlab.com/Spectrasonic/pixelart-redes/-/raw/master/redes-pixelart/redes-principal.png[/img]

[img title="img 2" width=200 height=81]https://gitlab.com/Spectrasonic/pixelart-redes/-/raw/master/redes-pixelart/follow.png[/img]

[url=http://spectrasonic.ml][img width=250 height=56]https://gitlab.com/Spectrasonic/pixelart-redes/-/raw/master/redes-pixelart/web-red.png[/img][/url]
[url=https://twitter.com/spectrasonic117][img width=250 height=56]https://gitlab.com/Spectrasonic/pixelart-redes/-/raw/master/redes-pixelart/Twitter-red.png[/img][/url]
[url=https://instagram.com/spectrasonic117][img width=250 height=56]https://gitlab.com/Spectrasonic/pixelart-redes/-/raw/master/redes-pixelart/instagram-red.png[/img][/url]
[url=https://twitch.tv/spectrasonic117][img width=250 height=56]https://gitlab.com/Spectrasonic/pixelart-redes/-/raw/master/redes-pixelart/twitch-red.png[/img][/url]

[right][size=18px]Made with [color=#e74c3c]❤[/color] by [style b color=#e74c3c]Spectrasonic[/style][/size][/right]
```


## In case of Optifine Submit


```

[img=Optifine Required]https://gitlab.com/Spectrasonic/pixelart-redes/-/raw/master/Optifine-pixelart/optifine-required.png[/img]

```